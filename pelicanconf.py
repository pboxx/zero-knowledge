#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'pj'
SITENAME = u'Zero Knowledge'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/London'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Non-academic', 'pages/non-academic-works.html'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Twitter', 'https://twitter.com/pedro_jac'),
          ('YouTube', 'https://www.youtube.com/channel/UC1Vcw2RoMNG4Ax1geNQYUsQ'),
          ('Gitlab', 'https://gitlab.com/pboxx'),
          ('Email', 'mailto:p.jacobetty@ed.ac.uk'))

DEFAULT_PAGINATION = 15

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

OUTPUT_PATH = 'public/'


# from voce theme

FUZZY_DATES = True

MANGLE_EMAILS = True

PLUGIN_PATHS=['voce/plugins']

PLUGINS=['assets']

THEME='voce/'
